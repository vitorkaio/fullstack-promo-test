from flask import Flask
import json
from src.database import Airport, Aircraft, Flight
import src.apiService as services
from src.services import haversine

app = Flask(__name__)

""" @app.route("/")
def hello():
  user = User(name="Alonso")
  res = user.save()
  return json.dumps({"res": res}) """

# Retorna todos os aeroportos
@app.route("/airports")
def airports():
  airports = Airport.select() # Verifica se os dados estão no banco de dados.

  if len(airports) == 0:
    airports = services.getAiports(4)
    for item in airports:
      airports_db = Airport(iata=item['iata'], city=item['city'], lat=item['lat'], lon=item['lon'], state=item['state'])
      res = airports_db.save()
    print('GET API')
  
  else:
    lis = []
    for item in airports:
      lis.append({"iata": item.iata, "city": item.city, "lat": item.lat, "lon": item.lon, "state": item.state})
    airports = lis

  if airports == None:
    return json.dumps({"error": 401})
  else:
    for index, item in enumerate(airports):
      for cont in range(0, len(airports)):
        # bloquear cal para o mesmo aeroporto
        if cont == index:
          pass
        else:
          flight = services.getFlight(item, airports[cont])
          distance = haversine(item['lon'], item['lat'], airports[cont]['lon'], airports[cont]['lat'])
          model = services.getFlightsCheap(flight, distance)
          # dep = item['iata']
          # arrival = airports[cont]['iata']
          # print(f'\n{dep} - {arrival} -> {distance}')
          # print(model)
          dep_ref = Airport.select().where(Airport.iata == item['iata']).get()
          arrival_ref = Airport.select().where(Airport.iata == airports[cont]['iata']).get()
          flights_db = Flight(departure=dep_ref.id, arrival=arrival_ref.id, distance=distance, api_mockup="www")
          print(flights_db)
    return json.dumps(airports)


if __name__ == "__main__":
  app.secret_key = 'm;4slF=Y6]Afb/.p9Xd7iO8(V0yU~R"'
  app.run(host='0.0.0.0', port=3333, threaded=True, debug=True)