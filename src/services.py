from math import radians, cos, sin, asin, sqrt
from datetime import datetime


def haversine(lon1, lat1, lon2, lat2):
  """
  Calculate the great circle distance between two points 
  on the earth (specified in decimal degrees)
  """
  # convert decimal degrees to radians 
  lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

  # haversine formula 
  dlon = lon2 - lon1 
  dlat = lat2 - lat1 
  a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
  c = 2 * asin(sqrt(a)) 
  r = 6371 # Radius of earth in kilometers. Use 3956 for miles
  dis = c * r
  return round(dis, 3)


def getSpeed(departure_time, arrival_time, distance):
  """ Retorna a velocidade da cada modelo de avião """

  fmt = "%Y-%m-%dT%H:%M:%S"
  d1 = datetime.strptime(departure_time, fmt)
  d2 = datetime.strptime(arrival_time, fmt)

  daysDiff = (d2-d1)

  # Convert to minutes
  minutesDiff = daysDiff.total_seconds() / 60
  speed = distance / (minutesDiff / 60)
  return round(speed, 3)


def getUniqueFare(distance, fare_total, speed):
  # time_route = (dis / speed) * 60
  
  unique = distance / fare_total
  # fare = fare_total + distance * unique  
  return round(unique, 3)

def price(distance, speed, fare, fare_unique):
  return fare + fare_unique * distance 

def time_route(distance, speed):
  return (distance / speed) * 60