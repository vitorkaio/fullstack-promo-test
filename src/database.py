from peewee import *
from playhouse.postgres_ext import PostgresqlExtDatabase

# postgre run docker, lembrar de criar a database no postgres
db = PostgresqlExtDatabase('mockup', user='postgres', password='123456', host='localhost', port=5432)

class Airport(Model):
  iata = CharField()
  city = CharField()
  lat = FloatField()
  lon = FloatField()
  state = CharField()
  class Meta:
    database = db # This model uses the "people.db" database.

class Flight(Model):
  departure = ForeignKeyField(Airport, backref='flights')
  arrival = ForeignKeyField(Airport, backref='flights')
  distance = FloatField()
  api_mockup = CharField()
  class Meta:
    database = db # This model uses the "people.db" database.

class Aircraft(Model):
  flight = ForeignKeyField(Flight, backref='aircrafts')
  price = FloatField()
  speed = FloatField()
  time = FloatField()
  model = CharField()
  manufacturer = CharField()
  class Meta:
    database = db # This model uses the "people.db" database.

db.connect()
db.create_tables([Airport, Flight, Aircraft])
