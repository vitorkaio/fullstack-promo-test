import requests as req
from requests.auth import HTTPBasicAuth
import json
import datetime
from .services import haversine, getSpeed, getUniqueFare, price, time_route

url_airports = 'http://stub.2xt.com.br/air/airports/rkiwlzxSLwlzwbbpfCpp3pCuruGJFnvO'
user = 'vitorcaio'
password = 'uhewlz'
apikey = 'rkiwlzxSLwlzwbbpfCpp3pCuruGJFnvO'
url_flights = f'http://stub.2xt.com.br/air/search/{apikey}'

# Retorna uma matriz com todos os aeroportos
def getAiports(qtd):
  res = req.get(url_airports, auth=HTTPBasicAuth(user, password))
  if res.status_code == 404 or res.status_code == 401:
    return None

  res = json.loads(res.text)
  airports = []
  for index, item in enumerate(res):
    if index == qtd:
      break
    else:
      airports.append(res[item])
  return airports

def getFlight(departure_airport, arrival_airport):
  # 40 dias após a execução
  nextDay_date = datetime.datetime.today() + datetime.timedelta(days=40)
  # nextDay_date.date()
  departure = departure_airport['iata']
  departure_lon = departure_airport['lon']
  departure_lat = departure_airport['lat']
  
  arrival = arrival_airport['iata']
  arrival_lon = arrival_airport['lon']
  arrival_lat = arrival_airport['lat']


  # Pega as informção do vôo e dos modelos de aviões dispoíveis
  res = req.get(f'{url_flights}/{departure}/{arrival}/{nextDay_date.date()}', auth=HTTPBasicAuth(user, password))
  if res.status_code == 404 or res.status_code == 401:
    return None
  
  # Calcula a distância em km entre os dois aeroportos
  return json.loads(res.text)

# flights
def getFlightsCheap(flights_info, distance):
  cheap = None
  for model in flights_info['options']:
    name = model['aircraft']['model']
    speed = getSpeed(model['departure_time'], model['arrival_time'], distance)
    fare_unique = getUniqueFare(distance, model['fare_price'], speed)
    price_total = price(distance, speed, model['fare_price'], fare_unique)
    time = time_route(distance, speed)

    if cheap == None:
      cheap = {
        "price": price_total,
        "speed": speed,
        "time": time,
        "model": name,
        "manufacturer": model['aircraft']['manufacturer']
      }
    else:
      if price_total < cheap['price']:
        cheap = {
        "price": price_total,
        "speed": speed,
        "time": time,
        "model": name,
        "manufacturer": model['aircraft']['manufacturer']
      }
  return cheap

